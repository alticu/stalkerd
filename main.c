#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

#include "platform/wm.h"

#ifndef POLL_RATE
# define POLL_RATE 1
#endif

bool running = true;
wm_win_t *current;

void exit_handler(int sig) {
  printf("Caught signal %d!\n", sig);
  running = false;
}

int main() {
    if (!wm_init()) {
        printf("Failed to initialize libstalker!\n");
        return 1;
    }

    struct sigaction act;
    act.sa_handler = exit_handler;
    sigaction(SIGINT, &act, NULL);
    sigaction(SIGQUIT, &act, NULL);

    current = malloc(sizeof(wm_win_t));
    wm_get_focused_window(current);

    printf("Starting stalkerd...\n");

    while (running) {
        wm_win_t *win = malloc(sizeof(wm_win_t));
        wm_get_focused_window(win);

        if (win->pid == current->pid) {
            if (strcmp(win->title, current->title) != 0) {
                printf("Title changed: %s\n", win->title);
                wm_win_free(current);
                current = win;
            }

            if (current != win)
                wm_win_free(win);
        } else {
            wm_win_free(current);
            current = win;

            printf("Started new session: %s\n", win->exe);
        }

      sleep(POLL_RATE);
    }

    printf("Goodbye!\n");
    wm_win_free(current);
    wm_cleanup();
    return 0;
}
