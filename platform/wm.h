/**
 * @file wm.h
 * @author joonatoona
 * @breif Interface for libstalker
 */

#ifndef STALKERD_WM_H
#define STALKERD_WM_H

#include <string.h>
#include <stdbool.h>

/**
 * @brief Window value container
 */
typedef struct window {
    char* title;    /**< Title of the window */
    char* program;  /**< Name of the program from the window */
    char* exe;      /**< Path to the programs executable */

    long pid;       /**< Native process ID of the window */
} wm_win_t;

/**
 * @breif Set up the stalker
 *
 * @return Sucess of the setup
 */
bool wm_init();

/**
 * @breif Clean up after the stalker
 *
 * Call this function after you're done using the stalker
 */
void wm_cleanup();

/**
 * @breif Get information about the current focused window
 *
 * @param target A pointer to a new wm_win_t object
 */
bool wm_get_focused_window(wm_win_t *target);

/**
 * @breif Free a wm_win_t object
 *
 * @param target A pointer to the wm_win_t object to free
 */
void wm_win_free(wm_win_t *target);

/**
 * @breif Get the amount of time the user has been idle
 *
 * @return The time in milliseconds since the user interacted with the computer
 */
unsigned long get_idle_time();

#endif //STALKERD_WM_H
