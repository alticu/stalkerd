//
// Created by joonatoona on 10/21/18.
//

#include "wm.h"

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/scrnsaver.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

Display* disp = NULL;

bool wm_init() {
    disp = XOpenDisplay(NULL);
    if (disp == NULL)
        return false;

    return true;
}

void wm_cleanup() {
    XCloseDisplay(disp);
}

unsigned char* get_prop(Window win, char* prop_name, unsigned long *nitems) {
    Atom type, filter;
    int fmt, status;
    unsigned long after;
    unsigned char* prop;

    filter = XInternAtom(disp, prop_name, true);
    status = XGetWindowProperty(disp, win, filter, 0, 1024, false, AnyPropertyType, &type, &fmt, nitems, &after, &prop);
    if (status != Success)
        return NULL;
    return prop;
}

bool wm_get_focused_window(wm_win_t *target) {
    if (disp == NULL)
        return false;

    int revert_to;
    Window win;
    XGetInputFocus(disp, &win, &revert_to);
    if (win == None)
        return false;

    // Check if window is real
    unsigned long n;
    XFree(get_prop(win, "WM_STATE", &n));
    if (n == 0) {
        // Window isn't being rendered, grab parent
        Window dummy, parent, *children = NULL;
        unsigned int n_c;
        XQueryTree(disp, win, &dummy, &parent, &children, &n_c);
        XFree(children);
        win = parent;
    }

    XTextProperty title;
    XGetWMName(disp, win, &title);
    target->title = (char*)title.value;

    XClassHint win_class;
    XGetClassHint(disp, win, &win_class);
    target->program = win_class.res_class;
    XFree(win_class.res_name);

    unsigned char* prop_r = get_prop(win, "_NET_WM_PID", &n);
    if (n != 0) {
        target->pid = *(long *)(prop_r);
        XFree(prop_r);

        char path[64];
        snprintf(path, 64, "/proc/%lu/exe", target->pid);

        target->exe = malloc(512);
        memset(target->exe, '\0', 512);
        readlink(path, target->exe, 512);
    } else {
        target->pid = -1;
        target->exe = malloc(1);
        target->exe[0] = '\0';
    }

    return true;
}

void wm_win_free(wm_win_t *target) {
    XFree(target->title);
    XFree(target->program);
    free(target->exe);
    free(target);
}

unsigned long get_idle_time() {
    XScreenSaverInfo *nfo = XScreenSaverAllocInfo();
    XScreenSaverQueryInfo(disp, DefaultRootWindow(disp), nfo);
    unsigned long t = nfo->idle;
    XFree(nfo);

    return t;
}
