# stalkerd

stalkerd is a library and daemon for monitoring which programs you use most.  
You can read the docs online at https://alticu.gitlab.io/stalkerd
